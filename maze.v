module maze(clk, starting_col, starting_row, maze_in, row, col, maze_oe, maze_we, done);
	input clk;
	input [5:0] starting_col, starting_row;
	input maze_in;
	output reg [5:0] row, col;
	output reg maze_oe;
	output reg maze_we;
	output reg done;
	
	reg initilized = 0;
	reg [5:0] pos_col = 10, pos_row = 10;
	reg mark = 0;
	
	reg started = 0;
	
	reg checking_down = 0;
	reg checked_down = 0;
	reg checking_up = 0;
	reg checked_up = 0;
	reg checking_right = 0;
	reg checked_right = 0;
	reg checking_left = 0;
	reg checked_left = 0;
	
	reg s_checking_down = 0;
	reg s_checked_down = 0;
	reg s_checking_up = 0;
	reg s_checked_up = 0;
	reg s_checking_right = 0;
	reg s_checked_right = 0;
	reg s_checking_left = 0;
	reg s_checked_left = 0;
	
	
	reg [1:0] dir = 0;
	
	always @(posedge clk) begin
		if (mark == 1) begin
			if (started == 0) begin
				maze_we <= 1;
				col <= starting_col;
				row <= starting_row;
				
				if (checking_down) begin
					if (maze_in == 0) begin
						//found direction down
						dir <= 0;
						started <= 1;
						pos_col <= starting_col;
						pos_row <= starting_row + 1;
					end
					else begin
						checking_right <= 1;
					end
					checked_down <= 1;
					checking_down <= 0;
				end
				else if (checking_right) begin
					if (maze_in == 0) begin
						//found direction right
						dir <= 1;
						started <= 1;
						pos_col <= starting_col + 1;
						pos_row <= starting_row;
					end
					else begin
						checking_up <= 1;
					end
					checked_right <= 1;
					checking_right <= 0;
				end
				else if (checking_up) begin
					if (maze_in == 0) begin
						//found direction right
						dir <= 2;
						started <= 1;
						pos_col <= starting_col;
						pos_row <= starting_row - 1;
					end
					else begin
						checking_left <= 1;
					end
					checked_up <= 1;
					checking_up <= 0;
				end
				if (checking_left) begin
					if (maze_in == 0) begin
						//found direction right
						dir <= 3;
						started <= 1;
						pos_col <= starting_col - 1;
						pos_row <= starting_row;
					end
					else begin
						checking_down <= 1;
					end
					checked_left <= 1;
					checking_left <= 0;
				end
				
				mark <= 0;
			end
			else begin
				if (s_checking_right) begin
					if (maze_in == 0 || maze_in == 2) begin
						//found direction right
						dir <= (dir == 0 ? 3 : dir - 1);
						started <= 1;
						pos_col <= pos_col + ((dir == 1 || dir == 3) ? 0 : ((dir == 2) ? 1 : -1));
						pos_row <= pos_row + ((dir == 0 || dir == 2) ? 0 : ((dir == 1) ? 1 : -1));
						maze_we <= 1;
						col <= pos_col + ((dir == 1 || dir == 3) ? 0 : ((dir == 2) ? 1 : -1));
						row <= pos_row + ((dir == 0 || dir == 2) ? 0 : ((dir == 1) ? 1 : -1));
					end
					else begin
						s_checking_up <= 1;
					end
					s_checked_right <= 1;
					s_checking_right <= 0;
				end
				else if (s_checking_up) begin
					if (maze_in == 0 || maze_in == 2) begin
						//found direction right
						dir <= dir;
						started <= 1;
						pos_col <= pos_col + ((dir == 1 || dir == 3) ? 0 : ((dir == 2) ? 1 : -1));
						pos_row <= pos_row + ((dir == 0 || dir == 2) ? 0 : ((dir == 1) ? 1 : -1));
						col <= pos_col + ((dir == 1 || dir == 3) ? 0 : ((dir == 2) ? 1 : -1));
						row <= pos_row + ((dir == 0 || dir == 2) ? 0 : ((dir == 1) ? 1 : -1));
					end
					else begin
						s_checking_left <= 1;
					end
					s_checked_up <= 1;
					s_checking_up <= 0;
				end
				if (s_checking_left) begin
					if (maze_in == 0 || maze_in == 2) begin
						//found direction right
						dir <= (dir == 3 ? 0 : dir + 1);
						started <= 1;
						pos_col <= pos_col + ((dir == 1 || dir == 3) ? 0 : ((dir == 2) ? 1 : -1));
						pos_row <= pos_row + ((dir == 0 || dir == 2) ? 0 : ((dir == 1) ? 1 : -1));
						maze_we <= 1;
						col <= pos_col + ((dir == 1 || dir == 3) ? 0 : ((dir == 2) ? 1 : -1));
						row <= pos_row + ((dir == 0 || dir == 2) ? 0 : ((dir == 1) ? 1 : -1));
					end
					else begin
						s_checking_down <= 1;
					end
					s_checked_left <= 1;
					s_checking_left <= 0;
				end
				else if (s_checking_down) begin
					if (maze_in == 0 || maze_in == 2) begin
						//found direction down
						dir <= (dir == 0 ? 2 : (dir == 1 ? 3 : (dir == 2 ? 0 : 1)));
						started <= 1;
						pos_col <= pos_col + ((dir == 1 || dir == 3) ? 0 : ((dir == 2) ? 1 : -1));
						pos_row <= pos_row + ((dir == 0 || dir == 2) ? 0 : ((dir == 1) ? 1 : -1));
						maze_we <= 1;
						col <= pos_col + ((dir == 1 || dir == 3) ? 0 : ((dir == 2) ? 1 : -1));
						row <= pos_row + ((dir == 0 || dir == 2) ? 0 : ((dir == 1) ? 1 : -1));
					end
					else begin
						s_checking_right <= 1;
					end
					s_checked_down <= 1;
					s_checking_down <= 0;
				end
				
				mark <= 0;
			end
		end
		else begin
			if (started == 0) begin
				//check for start direction
				if (checked_down == 0) begin
					checking_down <= 1;
					maze_oe <= 1;
					row <= starting_row + 1;
					col <= starting_col;
					mark <= 1;
				end
				else if (checked_right == 0) begin
					checking_right <= 1;
					maze_oe <= 1;
					row <= starting_row;
					col <= starting_col + 1;
					mark <= 1;
				end
				else if (checked_up == 0) begin
					checking_up <= 1;
					maze_oe <= 1;
					row <= starting_row - 1;
					col <= starting_col;
					mark <= 1;
				end
				else if (checked_left == 0) begin
					checking_left <= 1;
					maze_oe <= 1;
					row <= starting_row;
					col <= starting_col - 1;
					mark <= 1;
				end
			end
			else begin
				//TODO ce inseamna down in fucntie de dir
				if (s_checked_right == 0) begin
					s_checking_right <= 1;
					maze_oe <= 1;
					row <= pos_row + ((dir == 0 || dir == 2) ? 0 : ((dir == 1) ? 1 : -1));
					col <= pos_col + ((dir == 1 || dir == 3) ? 0 : ((dir == 2) ? 1 : -1));
					mark <= 1;
				end
				else if (s_checked_up == 0) begin
					s_checking_up <= 1;
					maze_oe <= 1;
					row <= pos_row + ((dir == 0 || dir == 2) ? 0 : ((dir == 1) ? 1 : -1));
					col <= pos_col + ((dir == 1 || dir == 3) ? 0 : ((dir == 2) ? 1 : -1));
					mark <= 1;
				end
				else if (s_checked_left == 0) begin
					s_checking_left <= 1;
					maze_oe <= 1;
					row <= pos_row + ((dir == 0 || dir == 2) ? 0 : ((dir == 1) ? 1 : -1));
					col <= pos_col + ((dir == 1 || dir == 3) ? 0 : ((dir == 2) ? 1 : -1));
					mark <= 1;
				end
				else if (s_checked_down == 0) begin
					s_checking_down <= 1;
					maze_oe <= 1;
					row <= pos_row + ((dir == 0 || dir == 2) ? 0 : ((dir == 1) ? 1 : -1));
					col <= pos_col + ((dir == 1 || dir == 3) ? 0 : ((dir == 2) ? 1 : -1));
					mark <= 1;
				end
			end
		end
		
		$display("col = %d row = %d pcol = %d prow = %d scol = %d srow = %d", col, row, pos_col, pos_row, starting_col, starting_row);
		if (pos_col == 0 || pos_row == 0 || pos_col == 63 || pos_row == 63) begin
			done <= 1;
		end
	end

endmodule